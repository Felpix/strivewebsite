#!/usr/bin/env python



#=====================|
#     APP IMPORTS     |
#                     |
#=====================|

import os
import sys
import pprint
from flask import Markup
from flask import redirect, url_for, session
from flask import redirect, render_template, request, session, abort
from sqlalchemy.orm import sessionmaker
from tabledef import *
from flask import Flask, render_template, flash, request
from wtforms import Form, TextField, TextAreaField, validators, StringField, SubmitField
import sqlite3 as lite
engine = create_engine('sqlite:///graypoint.db', echo=True)
from tabledef2 import *
from flask import g
import sqlite3
from flask_wtf import FlaskForm
from wtforms import *
from wtforms.validators import *
user_id="Ventavian"













#=====================|
#     APP CONFIG      |
#                     |
#=====================|

DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.database = "tutorial.db"
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'
#class ReusableForm(Form):
#    name = TextField('Name:', validators=[validators.required()])
#    email = TextField('Email:', validators=[validators.required(), validators.Length(min=6, max=35)])
#    password = TextField('Password:', validators=[validators.required(), validators.Length(min=3, max=35)])
Session = sessionmaker(bind=engine)
dsession = Session()

def connect_db():
    return sqlite3.connect(app.database)














#=====================|
#     APP ROUTES      |
#                     |
#=====================|




#    ----- REGISTER -----
#@app.route("/register", methods=['GET', 'POST'])
#def register():
#    form = ReusableForm(request.form)
#
#    if request.method == 'POST':
#        name=request.form['name']
#        password=request.form['password']
#        email=request.form['email']
#        print name, " ", email, " ", password, " ", coins
#
#        if form.validate():
#            flash('Thanks for registration ' + name)
#            #Session = sessionmaker(bind=engine)
#            #session = Session()
#
#            dsession.add(user)
#            dsession.commit()
#        else:
#            flash('Error: All the form fields are required. ')
#
#    return render_template('post.html', form=form)







#    ----- BROWSE -----
@app.route('/devblog', methods=['GET', 'POST'])
def browse():
    g.db = connect_db()
    cur = g.db.execute('SELECT * FROM Posts ORDER BY id DESC')
    posts = [dict(id=row[0], content=row[1], author=row[2], likes=row[3]) for row in cur.fetchall()]
    g.db.close()
    return render_template('browse.html', posts=posts)















#    ----- WRITE -----
@app.route('/makepost', methods=['GET', 'POST'])
def write():
    user_id="Ventavian"
    if not session.get('logged_in'):
        return redirect(url_for('login'))
    else:
        if request.method == 'POST':
            content = request.form['content']
            post = Posts(content, user_id)

            dsession.add(post)
            dsession.commit()
            return redirect('/browse')
        return render_template('write.html', user_id=user_id)








#    ----- MAIN -----
@app.route('/')
def home():
    return redirect(url_for('browse'))









#    ----- LOGIN -----
@app.route('/login', methods=['GET', 'POST'])
def login():
    POST_USERNAME = str(request.form['username'])
    POST_PASSWORD = str(request.form['password'])

    s = Session()
    query = s.query(User).filter(User.username==POST_USERNAME, User.password==POST_PASSWORD)
    result = query.first()
    if result:
        session['logged_in'] = True
    else:
        flash('wrong password!')
    return write()







#    ----- LOGOUT -----
@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()






#=====================|
#     NETWORKING      |
#                     |
#=====================|

if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
